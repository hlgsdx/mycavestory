#pragma once
#ifndef Map_h__
#define Map_h__
#include <vector>
#include <boost/shared_ptr.hpp>
#include "Game.h"
#include "Graphics.h"
#include "Sprite.h"
#include "Rectangle.h"

class Map
{
public:
    enum TileType {
        AIR_TILE,
        WALL_TILE
    };
    class CollisionTile {
    public:
        CollisionTile(int row, int col, TileType tile_type) :
        row(row), col(col),
            tile_type(tile_type) { }
        int row, col;
        TileType tile_type;
    };
    std::vector<CollisionTile> getCollidingTiles(const Rectangle& rectangle) const;
    static Map* createTestMap(Graphics& graphics);
    void update(int elapsed_time_ms);
    void draw(Graphics& graphics) const;
private:
    class Tile {
    public:
        Tile(TileType tile_type = AIR_TILE, boost::shared_ptr<Sprite> sprite=boost::shared_ptr<Sprite>()):
            tile_type(tile_type), sprite(sprite) { } 
        TileType tile_type;
        boost::shared_ptr<Sprite> sprite;
    };
    std::vector<std::vector<Tile> > tiles_;
};

#endif // Map_h__
