#pragma once
#ifndef CAVE_ANIMATESPRITE_H
#define CAVE_ANIMATESPRITE_H
#include "Game.h"
#include "Sprite.h"
class AnimatedSprite : public Sprite {
public:
    AnimatedSprite(Graphics &graphics, const std::string &file_path,
                  int source_x, int source_y,
                  int width, int height,
                  int fps, int num_frames);
    void Update(int elapsed_time_ms) override;
private:
    const int frame_time_; //each frame time (1000/fps)
    const int num_frames_; //animation frame
    int current_frame_;
    int elapsed_time_;
};
#endif //CAVE_ANIMATESPRITE_H
