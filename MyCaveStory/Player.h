#pragma once
#ifndef Player_h__
#define Player_h__
#include <boost/shared_ptr.hpp>
#include <cmath>
#include "Game.h"
#include "Graphics.h"
#include "Sprite.h"
#include "Map.h"
#include "Rectangle.h"
class Map;
class Player
{
public:
    Player(Graphics &graphics, int x, int y);
	~Player() { }
	void update(int elapsed_time_ms, const Map& map);
	void draw(Graphics &graphics);
	void startMovingLeft();
	void startMovingRight();
	void stopMoving();

private:
    enum MotionType {
        FIRST_MOTION_TYPE,
        STANDING=FIRST_MOTION_TYPE,
        WALKING,
        JUMPING,
        FALLING,
        INTERACTING,
        LAST_MOTION_TYPE
    };
    enum HorizontalFacing {
        FIRST_HORIZONTAL_FACING,
        LEFT=FIRST_HORIZONTAL_FACING,
        RIGHT,
        LAST_HORIZONTAL_FACING
    };
    enum VerticalFacing {
        FIRST_VERTICAL_FACING,
        UP=FIRST_VERTICAL_FACING,
        DOWN,
        HORIZONTAL,
        LAST_VERTICAL_FACING
    };
    class SpriteState { // Quote's facing pos, running state etc...
    public:
        SpriteState(MotionType motion_type = STANDING,
            HorizontalFacing horizontal_facing = LEFT,
            VerticalFacing vertical_facing = HORIZONTAL) :
            motion_type(motion_type),
            horizontal_facing(horizontal_facing),
            vertical_facing(vertical_facing) { }
        MotionType motion_type;
        HorizontalFacing horizontal_facing;
        VerticalFacing vertical_facing;
    };



    friend bool operator< (const SpriteState &a,const SpriteState &b); //states compare operator (used in std::map?)
    void initializeSprite(Graphics &graphics, const SpriteState& sprite_state);
    void initializeSprites(Graphics &graphics);
    SpriteState getSpriteState();
    Rectangle leftCollision(int delta) const;
    Rectangle rightCollision(int delta) const;
    Rectangle topCollision(int delta) const;
    Rectangle bottomCollision(int delta) const;
    void updateX(int elapsed_time_ms, const Map& map);
    void updateY(int elapsed_time_ms, const Map& map);
    int x_, y_;
    bool on_ground_;
	float acceleration_x_;
	float velocity_x_;
    float velocity_y_;
    bool jump_active_;
    bool interacting_;
    HorizontalFacing horizontal_facing_;
    VerticalFacing vertical_facing_;
    std::map<SpriteState, boost::shared_ptr<Sprite> > sprites_;
public:
    void startJump();
    void stopJump();
    bool on_ground() const { return on_ground_; }
    void lookUp();
    void lookDown();
    void lookHorizontal();
};


#endif // Player_h__

